# Atividade Prática

## Sprint 1

### Funcionamento do código calculadora.js

```bash
node calculadora.js <primeiro-termo> <segundo-termo>
```

O código inicialmente soma dois valores inteiros passados como argumentos na command line.

### Funcionamento do código calculadora.js de João

```bash
node calculadora.js <primeiro-termo> <operador> <segundo-termo>
```

Esse código realiza qualquer operação que utiliza operadores binários, aritméticos ou de comparação. Utiliza a função `eval()` do JavaScript que lê a string `(param1 + operator + param2)` e avalia seu valor.